package com.jira.driver;

/**
 * Technical class for browser's types
 */
public enum DriverType {
    IE("ie"),
    IE_REMOTE("ie"),
    FF("ff"),
    CHROME("chrome"),
    FF_REMOTE("ff"),
    CHROME_REMOTE("chrome");

    private String browserType;

    private DriverType(String browserType) {
        this.browserType = browserType;
    }

    public String getBrowserType() {
        return browserType;
    }

    public static DriverType getDriverTypeByBrowser(String browser) {
        for (DriverType driverType : DriverType.values()) {
            if (driverType.browserType.equals(browser)) {
                return driverType;
            }
        }
        return IE;
    }
}
