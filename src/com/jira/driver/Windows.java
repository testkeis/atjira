package com.jira.driver;

/**
 * Technical class for windows manager's holder
 */
public class Windows {

    private static ThreadLocal<WindowManager> holder = new ThreadLocal<>();

    public static void set(WindowManager windowManager) {
        holder.set(windowManager);
    }

    public static WindowManager get() {
        return holder.get();
    }
}
