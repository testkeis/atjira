package com.jira.driver;

import com.jira.util.data.DataProvider;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.Augmenter;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.net.MalformedURLException;
import java.net.URL;

/**
 * Technical class for browser's factory
 */
public enum DriversFactory {


    IE(DriverType.IE) {
        @Override
        protected WebDriver getDriverInstance() {
            DesiredCapabilities capabilities = DesiredCapabilities.internetExplorer();
            capabilities.setCapability(InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS, true);
            capabilities.setCapability("ignoreZoomSetting", true);
            capabilities.setJavascriptEnabled(true);

            capabilities.setCapability("nativeEvents", true);
            capabilities.setCapability("requireWindowFocus", true);
            capabilities.setCapability("browserAttachTimeout", 60000);
            capabilities.setCapability("unexpectedAlertBehaviour", "accept");
            return new InternetExplorerDriver(capabilities);
        }
    },


    FF(DriverType.FF) {
        protected WebDriver getDriverInstance() {
            WebDriver driver = new FirefoxDriver();
            return driver;
        }
    },

    CHROME(DriverType.CHROME) {
        protected WebDriver getDriverInstance() {
            ChromeOptions chromeOptions = new ChromeOptions();
            return new ChromeDriver(chromeOptions);
        }

    };

    protected abstract WebDriver getDriverInstance() throws MalformedURLException;

    private DriverType driverType;

    private DriversFactory(DriverType driverType) {
        this.driverType = driverType;
    }

    public static WebDriver getDriver(DriverType driverType) throws Exception {
        DriversFactory[] values = DriversFactory.values();
        for (DriversFactory value : values) {
            if (value.driverType.equals(driverType)) {
                return value.getDriverInstance();
            }
        }
        return IE.getDriverInstance();
    }
}
