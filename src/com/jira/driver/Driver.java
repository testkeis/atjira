package com.jira.driver;

import org.openqa.selenium.WebDriver;

/**
 * Technical class for Webdriver
 */
public class Driver {

    private static ThreadLocal<DriverData> holder = new ThreadLocal<>();

    public static void set(DriverData driverData) {
        holder.set(driverData);
    }

    public static WebDriver get() {
        return holder.get().getDriver();
    }

    public static String getMainWindowHandle() {
        return holder.get().getWindowHandle();
    }
}
