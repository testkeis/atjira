package com.jira.driver;

import com.jira.util.service.Wait;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.Deque;
import java.util.LinkedList;
import java.util.Set;

/**
 * Technical class to manage modal windows
 */
public class WindowManager {

    private Deque<String> deque = new LinkedList<>();

    public WindowManager(String mainWindowHandle) {
        deque.push(mainWindowHandle);
    }

    public void openDialog(WebElement openButton) {
        WebDriver driver = Driver.get();
        Set<String> windowHandlesBefore = driver.getWindowHandles();
        openButton.click();
        String newWindowHandle = Wait.waitForNewModalWindow(windowHandlesBefore);
        driver.switchTo().window(newWindowHandle);
        deque.push(newWindowHandle);
    }

    public void closeDialog() {
        deque.pop();
        Driver.get().close();
        Driver.get().switchTo().window(deque.peek());
    }

    public void closeDialog(WebElement closeButton) {
        deque.pop();
        WebDriver driver = Driver.get();
        Set<String> windowHandlesBefore = driver.getWindowHandles();
        closeButton.click();
        Wait.waitForCloseModalWindow(windowHandlesBefore);
        driver.switchTo().window(deque.peek());
    }
}
