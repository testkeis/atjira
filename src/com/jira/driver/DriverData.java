package com.jira.driver;

import org.openqa.selenium.WebDriver;

/**
 * Technical class for Driver's data
 */

public class DriverData {

    private WebDriver driver;
    private String windowHandle;

    public WebDriver getDriver() {
        return driver;
    }

    public void setDriver(WebDriver driver) {
        this.driver = driver;
    }

    public String getWindowHandle() {
        return windowHandle;
    }

    public void setWindowHandle(String windowHandle) {
        this.windowHandle = windowHandle;
    }
}
