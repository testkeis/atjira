package com.jira.pages.jira;

import com.jira.pages.Page;
import com.jira.util.RandomUtils;
import com.jira.util.service.Wait;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

/**
 * Represents Search results page elements and actions
 */
public class SearchResultsPage extends Page{

    @FindBy(id = "summary-val")
    private WebElement exactResultSummaryField;

    @FindBy(xpath = "//ul[@id='opsbar-edit-issue_container']/li/a[@id='edit-issue']")
    private WebElement editOption;

    private String bySummaryXpath = "//h1[@id='summary-val'][contains(., '$1')]";

    /**
     * Gets Summary field's text from Exact(opened with details)search result
     * @return Summary text
     */
    public String getExactResultSummaryFieldValue(){
        Wait.waitForElementDisplaying(By.id("summary-val"));
        return getText(exactResultSummaryField);
    }

    /**
     * Checks if Exact (opened) search result summary text matches the query's text
     * @param summary - query text
     * @return TRUE or FALSE
     */
    public boolean isIssueFound(String summary){
        return getExactResultSummaryFieldValue().contains(summary);
    }

    /**
     * clicks "Edit" option for opened test result
     */
    public void selectEditExactIssueOption(){
        clickAndWaitForDisplaying(editOption, By.id("edit-issue-submit"));
    }

    /**
     * waits for Summary text update if the summary was updated for opened issue
     * @param summary - new Summary text
     */
    public void waitForSummaryUpdate(String summary){
        Wait.waitForElementDisplaying(By.xpath(bySummaryXpath.replace("$1", summary)));
    }

    /**
     * Checks if the Summary's text was updated on Search Results page after it was changed in "Edit issue" dialog
     * @param summary - new Summary's text
     * @return TRUE or FALSE
     */
    public boolean isSummaryUpdated(String summary){
        try {
            waitForSummaryUpdate(summary);
        } catch (Exception e){
            return false;
        }
        return true;
    }

    /*public void selectEditExactIssueOptionJS(){
        JavascriptExecutor js = (JavascriptExecutor) Driver.get();
        js.executeScript("arguments[0].click();", editOption);
        Wait.waitForElementDisplaying(By.id("edit-issue-submit"));

    }*/
}
