package com.jira.pages.jira;

import com.jira.driver.Driver;
import com.jira.pages.Page;
import com.jira.util.service.Wait;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

/**
 * Created by kyrylo.keis on 7/02/14.
 */
public class LoginPage extends Page{

    @FindBy(id = "username")
    private WebElement emailField;

    @FindBy(id = "password")
    private WebElement passwordField;

    @FindBy(id = "login-submit")
    private WebElement loginButton;

    public void addEmail(String email){
        sendKeys(emailField,email);
    }

    public void addPassword(String password){
        sendKeys(passwordField,password);
    }

    public void clickLogin(){
        click(loginButton);
    }

    public void loginToJira(String email, String password) throws Exception{
        addEmail(email);
        addPassword(password);
        clickLogin();
        //Wait.waitForElementEnabling(By.id("create_link"));
        Wait.waitForPageLoaded(Driver.get());
    }
}
