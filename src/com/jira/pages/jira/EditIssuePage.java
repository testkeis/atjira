package com.jira.pages.jira;

import com.jira.driver.Driver;
import com.jira.pages.Page;
import com.jira.util.RandomUtils;
import com.jira.util.service.Wait;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

/**
 * Represents "Edit Issue" form elements and actions
 */
public class EditIssuePage extends Page{

    @FindBy(id = "summary")
    private WebElement summaryField;

    @FindBy(xpath = "//input[@id='edit-issue-submit']")
    private WebElement updateOption;


    /**
     * Clears Summary field
     */
    public void clearSummaryField(){
        clear(summaryField);
    }

    /**
     * Adds new Summary's text
     * @param summary - new Summary text
     * @return - added text to Summary field
     */
    public String addNewSummary(String summary){
        summary = summary+ RandomUtils.randomInt();
        sendKeys(summaryField, summary);
        return summary;
    }

    /**
     * clicks "Update" button
     */
    public void selectUpdateOption(){
        clickAndWaitForDisplaying(updateOption, By.xpath("//div[@id='details-module']"));
        Wait.waitForPageLoaded(Driver.get());
    }

    /**
     * Adds new Summary's text and clicks "Update" button
     * @param summary - New Summary text
     * @return - added text to Summary field
     */
    public String updateSummary(String summary){
        clearSummaryField();
        summary = addNewSummary(summary);
        selectUpdateOption();
        return summary;
    }
}
