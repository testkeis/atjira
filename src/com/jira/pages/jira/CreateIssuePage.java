package com.jira.pages.jira;

import com.jira.pages.Page;
import com.jira.util.RandomUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

/**
 * Represents "New Issue" form elements and actions
 */
public class CreateIssuePage extends Page{

    @FindBy(id = "summary")
    private WebElement summaryField;

    @FindBy(xpath = "//input[@id='project-field']")
    private WebElement projectField;

    @FindBy(xpath = "//input[@id='issuetype-field']")
    private WebElement issueTypeField;

    @FindBy(id = "create-issue-submit")
    private WebElement createOption;

    @FindBy(xpath = "//a[@class='cancel']")
    private WebElement cancelOption;

    @FindBy(xpath = "//a[@class='issue-created-key issue-link']")
    private WebElement createdIssueNotificationLink;

    /**
     * Adds text to "Summary" field
     * @param summary - text to be added to "Summary" field
     * @return "Summary" field
     */
    public String addSummary(String summary){
        summary = summary+RandomUtils.randomInt();
        sendKeys(summaryField, summary);
        return summary;
    }

    /**
     * Select the project from the project's list by name
     * @param name - required project's name
     */
    public void selectProject(String name){
        clear(projectField);
        sendKeys(projectField, name);
        clickAndWaitForDisplaying(projectField, By.xpath("//a[@title='A Test Project']"));
        clickAndWaitForEnabling(By.xpath("//a[@title='A Test Project']"), By.xpath("//input[@id='issuetype-field']"));
    }

    /**
     * Select "Bug" option from the issue's list
     */
    public void selectBugAsIssueType(){
        clickAndWaitForDisplaying(issueTypeField, By.xpath("//a[@title='Bug']"));
        clickAndWaitForEnabling(By.xpath("//a[@title='Bug']"), By.id("summary"));
    }

    /**
     * Clicks "Create" option
     */
    public void selectCreateOption(){
        clickAndWaitForDisplaying(createOption, By.xpath("//div[@class='aui-message success closeable']"));
    }

    /**
     * Fills mandatory fields and selects "Create" option
     * @param projectName - required project
     * @param summary - summary to be added
     * @return - added summary
     */
    public String addIssueWithMandatoryFields(String projectName, String summary){
        selectProject(projectName);
        selectBugAsIssueType();
        summary = addSummary(summary);
        selectCreateOption();
        return summary;
    }

    /**
     * Clicks "Cancel" button
     */
    public void selectCancelOption(){
        click(cancelOption);
    }

    /**
     * Checks if the issue created by waiting for confirmation notification and check if it contains new added summary
     * @param summary - added summary
     * @return - TRUE or FALSE
     */
    public boolean isIssueCreated(String summary){
        return isDisplayed(By.xpath("//div[@class='aui-message success closeable']")) && getCreatedIssueNotificationLink().contains(summary);
    }

    /**
     * Gets text from confirmation's notification link
     * @return - link's text
     */
    public String getCreatedIssueNotificationLink(){
        return getText(createdIssueNotificationLink);
    }

}
