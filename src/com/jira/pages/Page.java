package com.jira.pages;

import com.jira.Actions;
import com.jira.driver.Driver;
import org.apache.log4j.Logger;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import org.openqa.selenium.support.pagefactory.ElementLocatorFactory;

import java.awt.*;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;

/**
 * Parent class for pages
 */
 public abstract class Page extends Actions {

        protected CharSequence[] SPACE = new CharSequence[]{" "};

        public Page() {
            ElementLocatorFactory finder = new AjaxElementLocatorFactory(Driver.get(), 10);
            PageFactory.initElements(finder, this);
        }

        protected Logger log = Logger.getLogger(Page.class);
 }

