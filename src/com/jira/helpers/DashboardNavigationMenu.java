package com.jira.helpers;

import com.jira.Actions;
import com.jira.driver.Driver;
import com.jira.util.service.Wait;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;

/**
 * Represents Dashboard navigation menu elements and actions
 */

public class DashboardNavigationMenu extends Actions {

    public void selectLoginOption() {
        clickAndWaitForDisplaying(By.xpath("//a[contains(@class, 'login-link')]"), By.id("login-submit"));
    }

    public void selectLogoutOption() {
        clickAndWaitForDisplaying(By.xpath("//a[@id='header-details-user-fullname']"), By.xpath("//a[@id='log_out']"));
        clickAndWaitForDisplaying(By.xpath("//a[@id='log_out']"), By.xpath("//a[contains(@class, 'login-link')]"));
    }

    public void selectCreateOption() {
        Wait.waitForElementEnabling(By.xpath("//li[@id='create-menu']/a[@id='create_link']"));
        clickAndWaitForDisplaying(By.xpath("//li[@id='create-menu']/a[@id='create_link']"), By.xpath("//a[@class='cancel']"));
    }

    public void makeSearch(String query){
        clear(By.id("quickSearchInput"));
        sendKeys(By.id("quickSearchInput"), query);
        pressKeys(Keys.ENTER);
        Wait.waitForElementDisplaying(By.xpath("//a[@class='new-search aui-button']"));
        Wait.waitForPageLoaded(Driver.get());
    }
}
