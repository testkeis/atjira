package com.jira;

import com.jira.driver.Driver;
import com.jira.driver.Windows;
import com.jira.util.service.Wait;
import org.apache.log4j.Logger;
import org.openqa.selenium.*;
import org.openqa.selenium.internal.Locatable;

import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

/**
 * Technical class with Webdriver actions' wrapper
 */
public class Actions {

    protected void closeDialog(WebElement closeButton) {
        Windows.get().closeDialog(closeButton);
    }

    protected void closeDialog() {
        Windows.get().closeDialog();
    }

    protected void openDialog(WebElement openButton) {
        Windows.get().openDialog(openButton);
    }


    protected void click(By byElement) {
        find(byElement).click();
    }

    protected void click(WebElement element) {
        element.click();
    }

    protected void clear(By byElement) {
        find(byElement).clear();
    }

    protected void clear(WebElement element) {
        element.clear();
    }

    protected void sendKeys(By byElement, String keys) {
        find(byElement).sendKeys(keys);
    }

    protected void sendKeys(WebElement element, String keys) {
        element.sendKeys(keys);
    }

    protected String getText(By byElement){
        return find(byElement).getText();
    }

    protected String getText(WebElement element){
        return element.getText();
    }

    protected String getAttributeValue(By byElement){
        return find(byElement).getAttribute("value");
    }

    protected String getAttributeValue(WebElement element){
        return element.getAttribute("value");
    }

    protected String getAttributeText(By byElement){
        return find(byElement).getAttribute("text");
    }

    protected String getAttributeText(WebElement element){
        return element.getAttribute("text");
    }

    protected String getAttributeTitle(By byElement){
        return find(byElement).getAttribute("title");
    }

    protected String getAttributeTitle(WebElement element){
        return element.getAttribute("title");
    }

    protected String getAttributeLink(By byElement){
        return find(byElement).getAttribute("href");
    }

    protected String getAttributeLink(WebElement element){
        return element.getAttribute("href");
    }

    public void clickWithPause(By  byElement) {
        clickWithPause(find(byElement));
    }

    public void clickWithPause(WebElement element) {
        element.click();
        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            log.error(e);
        }
    }

    protected void clickAndWaitForEnabling(By byElement, By byEnabledElement) {
        clickAndWaitForEnabling(find(byElement), byEnabledElement);
    }

    protected void clickAndWaitForEnabling(WebElement element, By byEnabledElement) {
        element.click();
        Wait.waitForElementEnabling(byEnabledElement);
    }

    protected void clickAndWaitForDisplaying(By byElement, By byWaitForElement) {
        clickAndWaitForDisplaying(find(byElement), byWaitForElement);
    }

    protected void clickAndWaitForDisplaying(WebElement element, By byWaitForElement) {
        element.click();
        Wait.waitForElementDisplaying(byWaitForElement);
    }

    protected void clickAndWaitForStable(By byElement, By byWaitForElement) {
        clickAndWaitForStable(find(byElement), byWaitForElement);
    }

    protected void clickAndWaitForStables(By byElement, By byWaitForElement) {
        clickAndWaitForStables(find(byElement), byWaitForElement);
    }

    protected void submitAndWaitForStable(By byElement, By byWaitForElement) {
        submitAndWaitForStable(find(byElement), byWaitForElement);
    }

    protected void clickAndWaitForStable(WebElement element, By byWaitForElement) {
        element.click();
        Wait.waitForStableElement(byWaitForElement);
    }

    protected void submitAndWaitForStable(WebElement element, By byWaitForElement) {
        element.submit();
        Wait.waitForStableElement(byWaitForElement);
    }

    protected void clickAndWaitForStables(WebElement element, By byWaitForElement) {
        element.click();
        Wait.waitForStableElements(byWaitForElement);
    }

    protected void clickAcceptAlertAndWaitForStables(WebElement element, By byWaitForElement) {
        element.click();
        acceptAlert();
        Wait.waitForStableElements(byWaitForElement);
    }



    protected void doubleClick(By byElement) {
        org.openqa.selenium.interactions.Actions action = new org.openqa.selenium.interactions.Actions(Driver.get());
        action.doubleClick(find(byElement));
        action.perform();
    }

    protected void doubleClick(WebElement element) {
        org.openqa.selenium.interactions.Actions action = new org.openqa.selenium.interactions.Actions(Driver.get());
        action.doubleClick(element);
        action.perform();
    }


    protected boolean isDisplayed(By byElement) {
        try {
            return find(byElement).isDisplayed();
        } catch (Exception e) {
            return false;
        }
    }

    protected boolean isDisplayed(WebElement element) {
        try {
            return element.isDisplayed();
        } catch (Exception e) {
            return false;
        }
    }

    protected boolean isSelected(By byElement) {
        try {
            return find(byElement).isSelected();
        } catch (Exception e) {
            return false;
        }
    }

    protected boolean isSelected(WebElement element) {
        try {
            return element.isSelected();
        } catch (Exception e) {
            return false;
        }
    }

    protected boolean isElementPresent(By locator) {
        try {
            return find(locator) != null;
        } catch (Exception e) {
            return false;
        }
    }

    protected WebElement find(By by) {
        return Driver.get().findElement(by);
    }

    protected WebElement find(String xpath, String... params) {
        for (int i = 1; i <= params.length; i++) {
            xpath = xpath.replace("$"+i, params[i-1]);
        }
        return find(By.xpath(xpath));
    }

    protected List<WebElement> findMany(By by) {
        return Driver.get().findElements(by);
    }


    protected void pressKeys(Keys... keys) {
        org.openqa.selenium.interactions.Actions action = new org.openqa.selenium.interactions.Actions(Driver.get());
        action.sendKeys(keys);
        action.perform();
    }

    protected void acceptAlert() {
        Alert alert = Driver.get().switchTo().alert();
        alert.accept();
        Wait.waitForModalWindowDisappear();
    }

    protected String getAlertTextAndAccept() {
        Alert alert = Driver.get().switchTo().alert();
        String alertText = alert.getText();
        alert.accept();
        Wait.waitForModalWindowDisappear();
        return alertText;
    }

    protected String getAlertText() {
        Alert alert = Driver.get().switchTo().alert();
        String alertText = alert.getText();
        return alertText;
    }


    protected void dismissAlert() {
        Alert alert = Driver.get().switchTo().alert();
        alert.dismiss();
    }

    protected String getAlertTextAndDismiss() {
        Alert alert = Driver.get().switchTo().alert();
        String alertText = alert.getText();
        alert.dismiss();
        Wait.waitForModalWindowDisappear();
        return alertText;
    }


    protected void scrollTo(WebElement element) {
        ((Locatable) element).getCoordinates();
        ((JavascriptExecutor) Driver.get()).executeScript("arguments[0].scrollIntoView();", element);
    }

    protected void setImplicitlyWait(long seconds){
        Driver.get().manage().timeouts().implicitlyWait(seconds, TimeUnit.SECONDS);
    }

    public String getCookies(){
        Set<Cookie> cookies = Driver.get().manage().getCookies();
        StringBuilder cookieString = new StringBuilder();
        for (Cookie cookie : cookies) {
            cookieString.append(cookie.getName());
            cookieString.append("=");
            cookieString.append(cookie.getValue());
            cookieString.append(";");
        }
        return cookieString.toString();
    }

    private static Logger log = Logger.getLogger(Actions.class);
}
