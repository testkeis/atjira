package com.jira.verification.general;

import com.jira.SeleniumInit;
import com.jira.util.data.DataProvider;
import com.jira.util.data.entity.Issues;
import com.jira.util.data.entity.Projects;
import org.testng.Assert;
import org.testng.annotations.Test;

/**
 * General tests for Jira's issues
 */
public class JiraTest extends SeleniumInit{

    @Test(description = "As a user I want to create new issues")
    public void createBugWithMandatoryFieldsTest() throws Exception {
        Projects p = DataProvider.getProjects();
        Issues i = DataProvider.getIssues();
        dashNavMenu.selectCreateOption();
        String summary = pages.CREATEISSUE.addIssueWithMandatoryFields(p.getTestProject(), i.getBugSummary());
        //New issue's notification is displayed and contains correct data
        Assert.assertTrue(pages.CREATEISSUE.isIssueCreated(summary));
    }

    @Test(description = "As a user I want to search for issues")
    public void makeSearchForNewIssue() throws Exception {
        Projects p = DataProvider.getProjects();
        Issues i = DataProvider.getIssues();
        dashNavMenu.selectCreateOption();
        String summary = pages.CREATEISSUE.addIssueWithMandatoryFields(p.getTestProject(), i.getBugSummary());
        //Make Search using existing issue's Summary
        dashNavMenu.makeSearch(summary);
        //Search results "Summary" matches the query
        Assert.assertTrue(pages.SEARCHRESULTS.isIssueFound(summary));
    }

    @Test(description = "As a user I want to update existing issues")
    public void updateNewIssue() throws Exception {
        Projects p = DataProvider.getProjects();
        Issues i = DataProvider.getIssues();
        dashNavMenu.selectCreateOption();
        String summary = pages.CREATEISSUE.addIssueWithMandatoryFields(p.getTestProject(), i.getBugSummary());
        dashNavMenu.makeSearch(summary);
        pages.SEARCHRESULTS.selectEditExactIssueOption();
        //Update existing issue's Summary and remember it
        String summary1 = pages.EDITISSUE.updateSummary("Bug");
        //Opened issue's summary was correctly updated
        Assert.assertTrue(pages.SEARCHRESULTS.isSummaryUpdated(summary1));
        dashNavMenu.makeSearch(summary1);
        //It's possible to find the updated issue using new Summary as a query
        Assert.assertTrue(pages.SEARCHRESULTS.isIssueFound(summary1));
    }


}
