package com.jira;

import com.jira.driver.*;
import com.jira.helpers.DashboardNavigationMenu;
import com.jira.util.data.DataProvider;
import com.jira.util.data.entity.Credentials;
import com.jira.util.data.entity.Links;
import com.jira.util.service.Pages;
import com.jira.util.service.Wait;
import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.openqa.selenium.*;
import org.testng.ITestResult;
import org.testng.Reporter;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.lang.reflect.Method;
import java.util.concurrent.TimeUnit;

/**
 * Technical class to initialise Webdriver, default actions etc.
 */
public class SeleniumInit {

    protected Pages pages;
    protected DashboardNavigationMenu dashNavMenu;

    @BeforeMethod(alwaysRun = true)
    public void setUp(ITestResult result, Method method) {
        log.info("Setup for " + method.getDeclaringClass().getSimpleName() + "." + method.getName());
        WebDriver driver = null;
        try {
            driver = DriversFactory.getDriver(DriverType.FF);
            DriverData driverData = new DriverData();
            driverData.setDriver(driver);
            driverData.setWindowHandle(driver.getWindowHandle());
            Driver.set(driverData);
            Windows.set(new WindowManager(driver.getWindowHandle()));
            setupDriverProperties(driver);
            initializeServices();
            Login();

        } catch (Exception e) {
            log.error("----------------------------------------------");
            takeScreenshotWithRobot(result);
            log.error(e, e);
            e.printStackTrace();
            log.error("---------------------------------------------- Driver: " + driver);
            if (driver != null) {
                takeScreenshot(result);
                driver.quit();
            }
        }
    }

    @AfterMethod(alwaysRun = true)
    public void tearDown(ITestResult result, Method method) {
        String methodName = method.getDeclaringClass().getSimpleName() + "." + method.getName();
        takeScreenshot(result);
        Logout();
        log.info("Quit: " + methodName);
        try {
            WebDriver driver = Driver.get();
            driver.switchTo().window(Driver.getMainWindowHandle()).quit();
        } catch (Exception e) {
            log.error("Not possible to quit driver " + e.getMessage());
        }
        log.info("Finished " + methodName);
    }

    private void initializeServices() {
        pages = new Pages();
        dashNavMenu = new DashboardNavigationMenu();
    }

    private void setupDriverProperties(WebDriver driver) {
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
    }


    private void takeScreenshot(ITestResult result) {
        try {
            if (!result.isSuccess()) {
                File screenshot = new File(result.getMethod().getMethodName() + ".png");
                screenshot.delete();
                File screenshotTempFile = ((TakesScreenshot) Driver.get()).getScreenshotAs(OutputType.FILE);
                FileUtils.moveFile(screenshotTempFile, screenshot);
                Reporter.log("<a href='" + screenshot.getName() + "'>" +
                        result.getMethod().getMethodName() + " screenshot</a>");
            }
        } catch (Exception e) {
            takeScreenshotWithRobot(result);
            log.error(e, e);
        }
    }

    private void takeScreenshotWithRobot(ITestResult result) {
        try {
            BufferedImage image = new Robot().createScreenCapture(new Rectangle(Toolkit.getDefaultToolkit().getScreenSize()));
            File screenshot = new File(result.getMethod().getMethodName() + "-robot.png");
            ImageIO.write(image, "png", screenshot);
        } catch (Exception e) {
            log.error(e, e);
        }
    }

    public void navigatePage(String page){
        Driver.get().get(page);
    }

    public void Login() throws Exception{
        Links l = DataProvider.getLinks();
        Credentials c = DataProvider.getCredentials();
        navigatePage(l.getDashboardLink());
        dashNavMenu.selectLoginOption();
        pages.LOGIN.loginToJira(c.getLogin(), c.getPassword());
    }

    public void Logout(){
        dashNavMenu.selectLogoutOption();
    }

    protected Logger log = Logger.getLogger(this.getClass());
}
