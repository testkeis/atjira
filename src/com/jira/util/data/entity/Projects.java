package com.jira.util.data.entity;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Links Test data for xml binding
 */
    @XmlRootElement
    @XmlAccessorType(XmlAccessType.FIELD)
 public class
        Projects {

        @XmlElement
        private String testProject;

    public String getTestProject() {
        return testProject;
    }
}

