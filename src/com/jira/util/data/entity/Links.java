package com.jira.util.data.entity;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Issues Test data for xml binding
 */
    @XmlRootElement
    @XmlAccessorType(XmlAccessType.FIELD)
 public class
        Links {

        @XmlElement
        private String dashboardLink;

    public String getDashboardLink() {
        return dashboardLink;
    }
}

