package com.jira.util.data.entity;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Credentials Test data for xml binding
 */
    @XmlRootElement
    @XmlAccessorType(XmlAccessType.FIELD)
 public class Credentials {

        @XmlElement
        private String login;

        @XmlElement
        private String password;

        public String getLogin() {
            return login;
        }

        public String getPassword() {
            return password;
        }


 }

