package com.jira.util.data;

import com.jira.util.data.entity.*;
import org.apache.log4j.Logger;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import java.io.File;
import java.io.InputStream;

/**
 * Technical class for XML bindings, type conversion's etc.
 */
public class DataProvider {

    public static Links getLinks() {
        return (Links) getData(Data.LINKS);
    }

    public static Credentials getCredentials() {
        return (Credentials) getData(Data.CREDENTIALS);
    }

    public static Projects getProjects() {
        return (Projects) getData(Data.PROJECTS);
    }

    public static Issues getIssues() {
        return (Issues) getData(Data.ISSUES);
    }

    private static <T> T getData(Data data) {


        String xmlPath = buildXmlFilePath(data.fileName);
        try {
            Unmarshaller u = data.context.createUnmarshaller();
            InputStream resourceAsStream = DataProvider.class.getClassLoader()
                    .getResourceAsStream(xmlPath);
            Object resultObject = u.unmarshal(resourceAsStream);
            return (T) resultObject;
        } catch (Exception e) {
            log.error(e, e);
        }
        return null;
    }


    private static String buildXmlFilePath(String fileName) {
        return "data" + File.separator + File.separator + fileName;
    }


    enum Data {

        CREDENTIALS("Credentials.xml", Credentials.class),
        LINKS("Links.xml", Links.class),
        PROJECTS("Projects.xml", Projects.class),
        ISSUES("Issues.xml", Issues.class);
        private String fileName;
        private JAXBContext context;

        Data(String xmlPath, Class dataClass) {
            this.fileName = xmlPath;
            try {
                this.context = JAXBContext.newInstance(dataClass);
            } catch (JAXBException e) {
                log.error(e, e);
            }
        }
    }

    private static Logger log = Logger.getLogger(DataProvider.class);
}
