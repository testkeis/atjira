package com.jira.util.service;

import com.jira.pages.jira.CreateIssuePage;
import com.jira.pages.jira.EditIssuePage;
import com.jira.pages.jira.LoginPage;
import com.jira.pages.jira.SearchResultsPage;

/**
 * Technical class for pages' collection and initialisation
 */
public class Pages {

    public LoginPage LOGIN = null;
    public CreateIssuePage CREATEISSUE = null;
    public SearchResultsPage SEARCHRESULTS = null;
    public EditIssuePage EDITISSUE = null;


    public Pages() {
        LOGIN = new LoginPage();
        CREATEISSUE = new CreateIssuePage();
        SEARCHRESULTS = new SearchResultsPage();
        EDITISSUE = new EditIssuePage();
    }
}
