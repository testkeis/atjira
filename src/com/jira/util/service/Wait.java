package com.jira.util.service;

import com.jira.driver.Driver;
import com.google.common.base.Function;
import org.apache.log4j.Logger;
import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;
import java.util.Set;

/**
 * Technical class for explicit waits
 */
public class Wait {

    private static final int TIME_OUT_IN_SECONDS = 30;
    public static final int STABLE_ELEMENT_TIMEOUT = 30;

    public static WebElement waitForElement(By elementLocator) {
        WebDriverWait wait = new WebDriverWait(Driver.get(), TIME_OUT_IN_SECONDS);
        return wait.until(presenceOfElementLocated(elementLocator));
    }

    public static WebElement waitForElementEnabling(By elementLocator) {
        WebDriverWait wait = new WebDriverWait(Driver.get(), TIME_OUT_IN_SECONDS);
        return wait.until(enablingOfElementLocated(elementLocator));
    }

    public static WebElement waitForElementDisplaying(By elementLocator) {
        WebDriverWait wait = new WebDriverWait(Driver.get(), TIME_OUT_IN_SECONDS);
        return wait.until(displayingOfElementLocated(elementLocator));
    }

    public static void waitForModalWindowAppear() {
        WebDriverWait wait = new WebDriverWait(Driver.get(), TIME_OUT_IN_SECONDS);
        Set<String> wh = wait.until(presenceOfWindowCount(2));
        wh.remove(Driver.getMainWindowHandle());
        for (String handle : wh) {
            Driver.get().switchTo().window(handle);
        }
    }

    public static String waitForNewModalWindow(final Set<String> oldWindows) {
        WebDriverWait wait = new WebDriverWait(Driver.get(), TIME_OUT_IN_SECONDS);
        return wait.until(new ExpectedCondition<String>() {
            public String apply(WebDriver driver) {
                Set<String> allWindows = driver.getWindowHandles();
                allWindows.removeAll(oldWindows);
                return allWindows.size() > 0 ? allWindows.iterator().next() : null;
            }
        });
    }

    public static void waitForCloseModalWindow(final Set<String> oldWindows) {
        WebDriverWait wait = new WebDriverWait(Driver.get(), TIME_OUT_IN_SECONDS);
        wait.until(new ExpectedCondition<Boolean>() {
            public Boolean apply(WebDriver driver) {
                Set<String> allWindows = driver.getWindowHandles();
                return oldWindows.size()>allWindows.size();
            }
        });
    }

    public static void waitForModalWindowDisappear() {
        WebDriverWait wait = new WebDriverWait(Driver.get(), TIME_OUT_IN_SECONDS);
        wait.until(presenceOfWindowCount(1));
        Driver.get().switchTo().window(Driver.getMainWindowHandle());
    }

    private static Function<WebDriver, WebElement> presenceOfElementLocated(final By locator) {
        return new Function<WebDriver, WebElement>() {
            public WebElement apply(WebDriver driver) {
                return driver.findElement(locator);
            }
        };
    }

    private static Function<WebDriver, WebElement> enablingOfElementLocated(final By locator) {
        return new Function<WebDriver, WebElement>() {
            public WebElement apply(WebDriver driver) {
                WebElement element = driver.findElement(locator);
                try {
                    if (element.isEnabled()) {
                        return element;
                    }
                } catch (Exception e) {
                    log.error("enablingOfElementLocated: " + e.getMessage() + " for: " + locator.toString());
                    return null;
                }
                return null;
            }
        };
    }

    private static Function<WebDriver, WebElement> displayingOfElementLocated(final By locator) {
        return new Function<WebDriver, WebElement>() {
            public WebElement apply(WebDriver driver) {
                WebElement element = driver.findElement(locator);
                if (element.isDisplayed()) {
                    return element;
                }
                return null;
            }
        };
    }

    private static Function<WebDriver, Set<String>> presenceOfWindowCount(final int count) {
        return new Function<WebDriver, Set<String>>() {
            public Set<String> apply(WebDriver webDriver) {
                Set<String> windowHandles = webDriver.getWindowHandles();
                if (windowHandles.size() != count) {
                    return null;
                }
                return windowHandles;
            }
        };
    }

    public static Boolean waitForAjaxCompleted() {
        WebDriverWait wait = new WebDriverWait(Driver.get(), TIME_OUT_IN_SECONDS);
        return wait.until(new Function<WebDriver, Boolean>() {
            public Boolean apply(WebDriver webDriver) {
                return !(Boolean) ((JavascriptExecutor) webDriver).executeScript(
                        "return Ext.Ajax.isLoading()", new Object[]{});
            }
        });
    }

    public static WebElement waitForStableElement(final By locator) {
        return new WebDriverWait(Driver.get(), STABLE_ELEMENT_TIMEOUT).until(new ExpectedCondition<WebElement>() {
            public WebElement apply(WebDriver d) {
                try {
                    return d.findElement(locator);
                } catch (StaleElementReferenceException ex) {
                    log.error("waitForStableElement: " + ex.getMessage() + " for: " + locator.toString());
                    return null;
                }
            }
        });
    }

    public static List<WebElement> waitForStableElements(final By locator) {
        return new WebDriverWait(Driver.get(), STABLE_ELEMENT_TIMEOUT).until(new ExpectedCondition<List<WebElement>>() {
            public List<WebElement> apply(WebDriver d) {
                try {
                    List<WebElement> elements = d.findElements(locator);
                    if (!elements.isEmpty()) {
                        elements.get(0).getText();
                    }
                    return elements;
                } catch (StaleElementReferenceException ex) {
                    log.error("waitForStableElements: " + ex.getMessage() + " for: " + locator.toString());
                    return null;
                }
            }
        });
    }

    public static void waitForPageLoaded(WebDriver driver) {

        ExpectedCondition<Boolean> expectation = new
                ExpectedCondition<Boolean>() {
                    public Boolean apply(WebDriver driver) {
                        return ((JavascriptExecutor)driver).executeScript("return document.readyState").equals("complete");
                    }
                };

        WebDriverWait wait = new WebDriverWait(Driver.get(), TIME_OUT_IN_SECONDS);
        try {
            wait.until(expectation);
        } catch(Throwable error) {
           ;
        }
    }

    private static Logger log = Logger.getLogger(Wait.class);
}
