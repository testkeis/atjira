package com.jira.util;

import java.util.Random;

/**
 * Technical class for Random methods declaration
 */
public class RandomUtils {
    public static String randomName(String name) {
        Random random = new Random();
        int randomInt = random.nextInt(999999);
        return name + Integer.toString(randomInt);
    }

    public static int randomInt() {
        return randomInt(10000000);
    }

    public static int randomInt(int upperLimit) {
        Random random = new Random();
        return random.nextInt(upperLimit);
    }

    public static int randomSmallInt() {
        Random random = new Random();
        return random.nextInt(1000);
    }
}
